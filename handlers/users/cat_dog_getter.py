from aiogram import types
import random

from utils import dog_api, cat_api
from loader import dp


@dp.message_handler()
async def pw_generator(message: types.Message):
    cat_dog = random.randrange(0, 2)
    if cat_dog == 1:
        await message.answer_photo(photo=cat_api.cat_img())
    elif cat_dog == 0:
        await message.answer_photo(photo=dog_api.dog_img())
